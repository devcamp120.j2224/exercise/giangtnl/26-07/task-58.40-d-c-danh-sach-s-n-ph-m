package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Drink;

public interface iDrinkRespository extends JpaRepository<Drink , Long>{
    
}
